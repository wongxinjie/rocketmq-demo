import org.apache.rocketmq.acl.common.AclClientRPCHook;
import org.apache.rocketmq.acl.common.SessionCredentials;
import org.apache.rocketmq.client.AccessChannel;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

public class SyncProducer {

    public static void main(String []args) throws Exception{
        String groupId = "";
        String nameSrv = "";
        String topic = "";
        String accessKey = "";
        String secretKey = "";

        DefaultMQProducer producer = new DefaultMQProducer(
                groupId, new AclClientRPCHook(
                        new SessionCredentials(
                                accessKey,
                                secretKey
                        )));
        producer.setNamesrvAddr(nameSrv);
        producer.setAccessChannel(AccessChannel.CLOUD);

        producer.start();
        for (int i = 0; i < 10; i++) {
            Message msg = new Message(
                    topic,
                    "tag1",
                    ("Msg-Data-" + i).getBytes(RemotingHelper.DEFAULT_CHARSET)
                    );
            SendResult sendResult = producer.send(msg);
            System.out.printf("%s%n", sendResult);
        }
        producer.shutdown();
    }
}
